# Stack Docker Angular ( Node / Nginx )
### _angular-nginx-node_ 

## Builder & Lancer l'image
`docker image build -t angular-nginx-node .`

`docker run -p 3000:80 --rm angular-nginx-node`

_L'application sera alors exposé à l'adresse suivante : [localhost:3000](http://localhost:3000/)_


## Lancer depuis docker-compose
`docker-compose up`

_Permet de lancer la stack avec les paramètres présent dans le docker-compose_
