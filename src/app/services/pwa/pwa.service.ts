import {Injectable} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';


@Injectable()
export class PwaService {

  promptEvent;

  constructor(private swUpdate: SwUpdate) {
    swUpdate.available.subscribe(event => {
      if (this.askUserToUpdate()) {
        window.location.reload();
      }
      window.addEventListener('beforeinstallprompt', evnt => {
        this.promptEvent = evnt;
      });
    });
  }

  runInstall() {
    this.promptEvent.prompt();
  }

  /* TODO: fonction temporaire pour ne pas generer d'erreur. A remplacer par une modale */
  askUserToUpdate() {
    return false;
  }

}
