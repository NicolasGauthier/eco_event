import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  endpoint = 'http://localhost:3000/api/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getResponses(): Observable<any> {
    return this.http.get(this.endpoint + 'responses').pipe(
      map(this.extractData));
  }

  getResponse(id): Observable<any> {
    return this.http.get(this.endpoint + 'responses/' + id).pipe(
      map(this.extractData));
  }

  addResponse(response): Observable<any> {
    console.log(response);
    return this.http.post<any>(this.endpoint + 'responses', JSON.stringify(response), this.httpOptions).pipe(
      tap((response) => console.log(`added response w/ id=${response.id}`)),
      catchError(this.handleError<any>('addResponse'))
    );
  }

  updateResponse(id, response): Observable<any> {
    return this.http.put(this.endpoint + 'responses/' + id, JSON.stringify(response), this.httpOptions).pipe(
      tap(_ => console.log(`updated response id=${id}`)),
      catchError(this.handleError<any>('updateResponse'))
    );
  }

  deleteResponse(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'responses/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted response id=${id}`)),
      catchError(this.handleError<any>('deleteResponse'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
