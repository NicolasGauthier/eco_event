import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireService {
  endpoint = 'http://localhost:3000/api/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getQuestionnaires(): Observable<any> {
    return this.http.get(this.endpoint + 'questionnaires').pipe(
      map(this.extractData));
  }

  getQuestionnaire(id): Observable<any> {
    return this.http.get(this.endpoint + 'questionnaires/' + id).pipe(
      map(this.extractData));
  }

  addQuestionnaire(questionnaire): Observable<any> {
    console.log(questionnaire);
    return this.http.post<any>(this.endpoint + 'questionnaires', JSON.stringify(questionnaire), this.httpOptions).pipe(
      tap((questionnaire) => console.log(`added questionnaire w/ id=${questionnaire.id}`)),
      catchError(this.handleError<any>('addQuestionnaire'))
    );
  }

  updateQuestionnaire(id, questionnaire): Observable<any> {
    return this.http.put(this.endpoint + 'questionnaires/' + id, JSON.stringify(questionnaire), this.httpOptions).pipe(
      tap(_ => console.log(`updated questionnaire id=${id}`)),
      catchError(this.handleError<any>('updateQuestionnaire'))
    );
  }

  deleteQuestionnaire(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'questionnaires/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted questionnaire id=${id}`)),
      catchError(this.handleError<any>('deleteQuestionnaire'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
