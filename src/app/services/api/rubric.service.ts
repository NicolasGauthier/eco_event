import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RubricService {
  endpoint = 'http://localhost:3000/api/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getRubrics(): Observable<any> {
    return this.http.get(this.endpoint + 'rubrics').pipe(
      map(this.extractData));
  }

  getRubric(id): Observable<any> {
    return this.http.get(this.endpoint + 'rubrics/' + id).pipe(
      map(this.extractData));
  }

  addRubric(rubric): Observable<any> {
    console.log(rubric);
    return this.http.post<any>(this.endpoint + 'rubrics', JSON.stringify(rubric), this.httpOptions).pipe(
      tap((rubric) => console.log(`added rubric w/ id=${rubric.id}`)),
      catchError(this.handleError<any>('addRubric'))
    );
  }

  updateRubric(id, rubric): Observable<any> {
    return this.http.put(this.endpoint + 'rubrics/' + id, JSON.stringify(rubric), this.httpOptions).pipe(
      tap(_ => console.log(`updated rubric id=${id}`)),
      catchError(this.handleError<any>('updateRubric'))
    );
  }

  deleteRubric(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'rubrics/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted rubric id=${id}`)),
      catchError(this.handleError<any>('deleteRubric'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
