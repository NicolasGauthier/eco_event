import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConnectpageComponent} from './connection/connectpage/connectpage.component';
import {InformationComponent} from './connection/information/information.component';
import {EventComponent} from './event/event.component';
import {ChooseProfilComponent} from './choose-profil/choose-profil.component';
import {ExplorerComponent} from './explorer/explorer.component';

const routes: Routes = [
  { path: 'login', component: ConnectpageComponent },
  { path: 'info', component: InformationComponent },
  { path: 'events', component: EventComponent },
  { path: 'explorer', component: ExplorerComponent },
  { path: 'choose-profil', component: ChooseProfilComponent },
  { path: '',
    redirectTo: '/info',
    pathMatch: 'full'
  },
  { path: '**', component: EventComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
