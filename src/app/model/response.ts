export class Response {
  id: number;
  questionnaire: string;
  question: string;
  value: string;
}
