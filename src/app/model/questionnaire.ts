import {Response} from './response';

export class Questionnaire {
  id: number;
  user: string;
  createdAt: Date;
  responses: Array<Response>;
  event: string;
}
